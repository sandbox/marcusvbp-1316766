This module aims to hide some elements located in the user's editing account form and content creation form. These elements have little or no use in sites that have only one content editor, or have several editors but there is no need for version and authorship control.

The module will also add permissions, allowing you to display hidden elements for certain user roles and content types.

Use this module when you need to:

1.Lock the personal contact form settings: this module will remove the fieldset "contact options" in user account editing form, preventing users from enabling or disabling the personal contact form;

2. Apply "administer content" permission  to a user role, but remove the version and authorship control: sometimes you want to apply the permission to manage content for a user role. This allows the user with this role to control the options of content publication he is creating or editing, but at the same time it will display options for revision control, and also content authorship. These options are useful in projects where the same content is handled by various publishers. In simpler commercial web sites, which have only one or a few publishers, such feature is totally expendable.
